import React from 'react';
import axios from 'axios';
import Config from '../components/config';
import ReactLoading from 'react-loading';
import moment from 'moment';
import {Link} from "react-router-dom";
import LoanEligibilityQuickPay from "./LoanEligibilityQuickPay";
import { parse } from 'webpack/lib/Parser';



class QuickPayContainer extends React.Component{
    constructor(props){
        super(props);
        // console.log("Props in container::: ", props);
        this.state={
            seller_token:props ? props.seller_token:"",
            seller_data:props ? props.seller_data:"",
            seller_data_all:props ? props.seller_data_all:"",
        }
    }
    componentWillReceiveProps(nextProps){
        this.setState({
            seller_token:nextProps ? nextProps.seller_token : "",
            seller_data:nextProps ? nextProps.seller_data : "",
            seller_data_all:nextProps ? nextProps.seller_data_all : "",
        })
    }

	render(){
		return(
                <div class="page-wrapper">
                    <Title seller_token={this.state.seller_token} seller_data_all = {this.state.seller_data_all} />

                    <GraphContainer seller_data = {this.state.seller_data} seller_data_all = {this.state.seller_data_all} seller_token={this.state.seller_token} />
                    {/*<RecentOrders seller_token={this.state.seller_token} />*/}

               </div>
		);
	}
}
class Title extends React.Component{

    constructor(props){
        super(props);
        // console.log("props dashboard ||| ",props);
        this.state={
            sync_loader: false,
           // sync_props:false,
            seller_token:props.seller_token?props.seller_token:"",
            seller_name:props.seller_name?props.seller_name:"",
            seller_data_all:props ? props.seller_data_all:"",
        }
        this.syncPayment = this.syncPayment.bind(this);
    }

    //sync Payments page from database
    syncPayment(){
        this.setState({sync_loader: true});
        // console.log("from line 53 container", this.state.seller_token);
        var auth = "Token "+ this.state.seller_token;
        let sync_payment_url = "/sp-api/seller-partner-finance/";
        axios.get(sync_payment_url, {"headers": {'Authorization': auth}})
        .then(res => {
            this.setState({sync_loader:false});
        }).catch((err) => {
            this.setState({sync_loader:false});
        });
    }
    componentWillReceiveProps(nextProps){

        this.setState({
            sync_props : nextProps.sync_perfor?nextProps.sync_perfor:false,
            seller_token : nextProps.seller_token?nextProps.seller_token:"",
            seller_name : nextProps.seller_name?nextProps.seller_name:"",
            seller_data_all:nextProps ? nextProps.seller_data_all : "",
        })

    }
	render(){
		return(
            <div class="page-title">
                <div class="row align-items-center">
                    <div class="col-sm-6">
                        <h2 class="page-title-text">Quick Pay Dashboard</h2>
                    </div>

                </div>
            </div>
		);
	}
}
class GraphContainer extends React.Component{
	constructor(props){
		super(props);
            this.state={
                seller_token:props.seller_token?props.seller_token:"",
                seller_data:props?props.seller_data:"",
                seller_data_all:props?props.seller_data_all:"",

            }


	};
	componentWillReceiveProps(nextProps){
	    this.setState({
             seller_token:nextProps.seller_token?nextProps.seller_token:"",
             seller_data:nextProps?nextProps.seller_data:"",
             seller_data_all:nextProps?nextProps.seller_data_all:"",

        })
    }


	render(){
	    var seller_type = this.state.seller_data ? this.state.seller_data:"";
	    // console.log("seller_type", seller_type);
	    // console.log("seller_data", this.state.seller_data);
	    let loan_screen = []
	    if (this.state.seller_data === 'QuickPay'){
            loan_screen.push(<LoanEligibilityQuickPay seller_data={this.state.seller_data} seller_token={this.state.seller_token}/>)
        }
		return(
		        <div class="page-body">
                    {/*<LoanHistory seller_data={this.state.seller_data} seller_data_all={this.state.seller_data_all} seller_token={this.state.seller_token}/>*/}
                    <ConsolidateFigures seller_data={this.state.seller_data} seller_data_all={this.state.seller_data_all} seller_token={this.state.seller_token}/>
                    {/*{loan_screen}*/}

                </div>

		);
	}
}

class ConsolidateFigures extends React.Component{
	constructor(props){
		super(props);
        this.state = {
            data: [],
            loan: [],
            loan_balance: [],
            loan_history: [],
            payment_success_history: [],
            isLoading:true,
            error_msg:"",
            api_key:"",
            password:"",
            shop_name:"",
            account_list:[],
            seller_token : props ? props.seller_token :"",
            seller_data : props ? props.seller_data :"",
            seller_data_all : props ? props.seller_data_all :"",
        }
	}

	componentWillReceiveProps(nextProps){
	    this.setState({
            seller_token : nextProps ? nextProps.seller_token :"",
            seller_data : nextProps ? nextProps.seller_data :"",
            seller_data_all : nextProps ? nextProps.seller_data_all :""
        })
    }
	componentWillMount() {
	    var auth = "Token "+ this.state.seller_token;
	    var url="/api/loan-amount/";
	    // console.log("seller_data_all in container :::::: ", this.state.seller_data_all);
	    var seller_type = this.state.seller_data;

	    // console.log("seller_data in container :::::: ", this.state.seller_data);
	    if (seller_type=="QuickPay"){
	        url="/api/qpayloan-amount/";
        }else{
	        url="/api/loan-amount/";
        }
        axios.get(url,{ 'headers': { 'Authorization': auth } })
            .then(res => {
                this.setState({
                    data: res.data.results[0],
                    loan: res.data.results[0].loan,
                    loan_balance: res.data.results[0].loan_balance,
                    loan_history: res.data.results[0].loan_history,
                    payment_success_history: res.data.results[0].payment_success_history,
                    isLoading:false,
                });
            }).catch((err) => {
            this.setState({
                    isLoading:false,
            });
        });

	     // let auth = "Token "+ this.state.seller_token;

         axios.get('/api/v1/shopify_account/',{ 'headers': { 'Authorization': auth }})
            .then(res => {
                // console.log("results shopify account::::::::::::",res.data.results);
                this.setState({
                    account_list:res.data.results
                });
            }).catch((err) => {
                this.setState({
                    isLoading:false,
                });
             // console.log("error", err);
        });
    }

    componentDidMount() {
	    var auth = "Token "+ this.state.seller_token;
	    axios.get('/api/client-details/',{ 'headers': { 'Authorization': auth }})
            .then(res => {
                this.setState({
                    seller_data_all:res.data.data
                });
            }).catch((err) => {
                this.setState({
                    isLoading:false,
                });
        });

    }

    updatePrimary(e){
         e.preventDefault();
        var id= e.currentTarget.dataset.id;
        var url=document.location.origin+"/api/v1/seller_plaid_accounts/"+id+"/";
        var auth="Token "+localStorage.getItem('key');
        fetch(url,{
            method:"PATCH",
            headers: {
                        'Accept': 'application/json',
                        'Content-Type': 'application/json',
                        'Authorization': auth
                         },
            credentials: 'same-origin',
             body:JSON.stringify({
                id:id,
                is_primary:true

            })
        }).then(response=>{
            if(response.status==200){
                // var lasturl = location.href ;
                // document.location.href=newurl;
                location.reload();

            }
            else{
                // console.log("Updated Loan Status error", response);

            }
        });
    }

   changeAPIKey(e){
       // console.log("changename ::: ",e.target.value);
       this.setState({
           api_key:e.target.value
       });
   }
   changePassword(e){
       this.setState({
           password:e.target.value
       })
   }
   changeShopName(e){
       this.setState({
           shop_name:e.target.value
       })
   }
   getCookie(name) {
            var cookieValue = null;
            if (document.cookie && document.cookie !== '') {
                var cookies = document.cookie.split(';');
                for (var i = 0; i < cookies.length; i++) {
                    var cookie = jQuery.trim(cookies[i]);
                    if (cookie.substring(0, name.length + 1) === (name + '=')) {
                        cookieValue = decodeURIComponent(cookie.substring(name.length + 1));
                        break;
                    }
                }
            }
        return cookieValue;
        }
   submitHandler(e){
       // console.log("In submit Handler :::::");
       var api_key = this.state.api_key;
       var password = this.state.password;
       var shop_name = this.state.shop_name;
       var error = 0;
       if (api_key == "") {
           this.setState({
               error_msg: "Shopify API Key cannot be left blank"
           });
           error = 1;
       } else if (password == "") {
           this.setState({
               error_msg: "Shopify password cannot be left blank"
           });
           error = 1;
       } else if (shop_name == "") {
           this.setState({
               error_msg: "Shop name cannot be left blank"
           });
           error = 1;
       } else {
           this.setState({
               error_msg: ""
           });
           error = 0;
       }
       if (error == 0) {
           // console.log("Submit Success....");
           let auth = "Token " + this.state.seller_token;
           let csrftoken = this.getCookie('csrftoken');

           // console.log("Auth ::::: ", auth);
           // this.getnewToken();
           var url = "/api/v1/shopify_account/"
           fetch(url, {
               method: "POST",
               headers: {
                   'Accept': 'application/json',
                   'Content-Type': 'application/json',
                   'Authorization': auth,
                   'X-CSRFToken': csrftoken
               },
               credentials: 'same-origin',
               body: JSON.stringify({
                   api_key: api_key,
                   password: password,
                   shop_name: shop_name,
                   is_primary: true
               })
           }).then(response => {
               // console.log("shopify Account req status", response);
               if (response.status == 201 || response.status == 200) {
                   // console.log(response.json());
                   location.reload();
               } else {
                   // console.log("Account create error", response);
                   this.setState({
                       error_msg: response.statusText
                   })

               }
           });
       }
   }

      submitMarketplace(e){
	    // console.log("success");
	    let amz_seller = 'https://sellercentral.amazon.com/apps/authorize/consent';
            let app_param = '?application_id=';
            let app_id = 'amzn1.sellerapps.app.4c357c5e-5f06-49fd-aaca-c5d70d4f5582';
            let state = '&state=';
            // let state_id = Math.floor(Math.random() * 10000000000000000);
            let state_id = this.state.seller_token;
            let version = '&version=beta';
            // console.log("token", state_id)
            // localStorage.setItem('state', ''+state_id);
            window.location.href = amz_seller + app_param + app_id + state + state_id + version;

   }

	render(){
	    var balance=[];
	    var loan_fee_per=[];
	    var amount_approve=[];
	    var last_payment=[];
	    var lstatus=[];
	    var payment_amz_payment=[];
	     let error=[];
       // console.log("error msg::::::", this.state.error_msg);
       if(this.state.error_msg !==""){
            error.push(
                <div className="alert alert-danger">
                    {this.state.error_msg}
                </div>
            )

        }
       var shopify_account_status = [];
       var shopify_account=[];

       if(this.state.account_list.length > 0){
           shopify_account.push(
               <ul>
                   <li>API Key: {this.state.account_list[0] ? this.state.account_list[0].api_key : "NA"}</li>
                   <li>Date Connected: {this.state.account_list[0] ?  moment(this.state.account_list.date_created).format('DD MM YYYY') : "NA"}</li>
                   <li className="text-success">Connected</li>
               </ul>
           );
           shopify_account_status.push(
               <i className="icon-check text-success download"
                  style={{"font-size": "40px"}} />
           )
       }else{

           shopify_account.push(
               <ul>
                   <li>Seller ID: N/A</li>
                   <li>Date Connected: N/A</li>
                   <li className="text-warning">Not Connected</li>
               </ul>
           );
           shopify_account_status.push(
               <i className="icon-plus text-warning download"  data-toggle="modal"
                   data-target="#modal4"
                   style={{"font-size": "40px"}} />
           )
       }
       console.log("seller_data_all  :::: ", this.state.seller_data_all);
	    var loan_status = this.state.seller_data_all.status;
        var adv_rate = this.state.seller_data_all.adv_rate;
        let loan_target_percentage = this.state.loan_balance.target_percentage ? this.state.loan_balance.target_percentage:0;
	    let loan_history_percentage = this.state.loan_history.history_target_percentage ? this.state.loan_history.history_target_percentage : 0;
	    let payment_percentage = this.state.payment_success_history.target_percentage ? this.state.payment_success_history.target_percentage:0;
        if(this.state.isLoading){
	        balance.push(
	            <ReactLoading type={'spin'} color={'blue'} height={'30%'} width={'30%'} />
            );
            loan_fee_per.push(
	            <ReactLoading type={'spin'} color={'blue'} height={'30%'} width={'30%'} />
            );
	        amount_approve.push(
	            <ReactLoading type={'spin'} color={'blue'} height={'30%'} width={'30%'} />
            );
	        last_payment.push(
	            <ReactLoading type={'spin'} color={'blue'} height={'30%'} width={'30%'} />
            );
	        payment_amz_payment.push(
	            <ReactLoading type={'spin'} color={'blue'} height={'30%'} width={'30%'} />
            );
	        lstatus.push(
	            <ReactLoading type={'spin'} color={'blue'} height={'30%'} width={'30%'} />
            );


        }else{
	        // console.log(loan_status, "====loan_status");
	        if(loan_status === 'Loan_active') {
                    lstatus.push(<p className="tbl-cell text-right text-success">Active</p>)
                }
                else if(loan_status === 'Pending') {
                     lstatus.push(<p className="tbl-cell text-right ">Pending</p>)
                    // status.push("Pending")
                }
                else if(loan_status === 'Approved') {
                    lstatus.push(<p className="tbl-cell text-right text-success">Active</p>)
                   // status.push("Active")
               }
                 else if(loan_status === 'Closed') {
                    lstatus.push(<p className="tbl-cell text-right">Closed</p>)
                     // status.push("Closed")
                }
                else if(loan_status === 'Enquiry') {
                    lstatus.push(<p className="tbl-cell text-right ">Enquiry</p>)
                    // status.push("Enquiry")
                }
                else if(loan_status === 'Prequalified') {
                    lstatus.push(<p className="tbl-cell text-right ">Active</p>)
                    // status.push("Active")
                }else{
                    lstatus.push(<p className="tbl-cell text-right "></p>)
                }
            console.log("||| balance", this.state.data)
	        balance.push(
	            <p class="tbl-cell text-right balance">$ {this.state.loan.balance ? (this.state.loan.balance).toLocaleString('en') :"0.00"}</p>
            );
            loan_fee_per.push(
	            <p class="tbl-cell text-right balance"> {this.state.seller_data_all.loan_fee_percentage ? (this.state.seller_data_all.loan_fee_percentage).toLocaleString('en') +" %" :"0.00"}</p>
            );

	        amount_approve.push(
	            <p class="tbl-cell text-right">$ {this.state.data.amount_approve ? (this.state.data.amount_approve).toLocaleString('en') :"0.00"}</p>
            );
            
            last_payment.push(
	            <p class="tbl-cell text-right">$ {this.state.loan.last_payment ? (this.state.loan.last_payment).toLocaleString('en') :"0.00"}</p>
            );

            payment_amz_payment.push(
	            <p class="tbl-cell text-right">$ {this.state.loan.payment_amz_payment? (this.state.loan.payment_amz_payment).toLocaleString('en') :"0.00"}</p>
            );

        }
		return(
		        <React.Fragment>
                    <Popup/>
                    <div className="row">
                        <div className="col-sm-12 col-md-6 col-lg-3">
                            <div className="icon-widget">
                                <h5 className="icon-widget-heading">Account Status</h5>
                                <div className="icon-widget-body tbl">
                                    <p className="tbl-cell col-lg-2"><i className="icon-power text-success"></i></p>
                                    {lstatus}
                                </div>
                            </div>
                        </div>
                        <div className="col-sm-12 col-md-6 col-lg-3">
                            <div className="icon-widget">
                                <h5 className="icon-widget-heading">Loan Product</h5>
                                <div className="icon-widget-body tbl">
                                    <p className="tbl-cell balance-icon col-lg-2"><i className="icon-layers text-dark"></i></p>
                                    {/*<p className="tbl-cell text-right">{balance}</p>*/}
                                    <p className="tbl-cell text-right col-lg-10">Daily</p>
                                </div>
                            </div>
                        </div>
                        <div className="col-sm-12 col-md-6 col-lg-3">
                            <div className="icon-widget">
                                <h5 className="icon-widget-heading">Advance Rate</h5>
                                <div className="icon-widget-body tbl">
                                    <p className="tbl-cell col-lg-2"><i className="icon-wallet text-success"></i></p>
                                    <p className="tbl-cell text-right col-lg-10">{adv_rate}%</p>
                                </div>
                            </div>
                        </div>
                        <div className="col-sm-12 col-md-6 col-lg-3">
                            <div className="icon-widget">
                               <h5 className="icon-widget-heading">Current Fee Rate</h5>
                                <div className="icon-widget-body tbl">
                                    <p className="tbl-cell col-lg-1"><i className="icon-wallet text-success"></i></p>
                                    <p className="tbl-cell text-right col-lg-11">{loan_fee_per}</p>
                                </div>
                            </div>
                        </div>
                    </div>

                  <LoanHistory seller_data={this.state.seller_data} seller_data_all={this.state.seller_data_all} seller_token={this.state.seller_token}/>

                  <div className="row   pt-4">

                        <div className="col-md-8">
                            <div className="panel panel-default">

                                <div className="modal fade" id="modal4">
                                    <div className="modal-dialog">
                                        <div className="modal-content">
                                            <div className="modal-header">
                                                <h4 className="modal-title">Add Shopify Account</h4>
                                                <button type="button" className="close" data-dismiss="modal"
                                                        aria-label="Close"><span aria-hidden="true">×</span></button>
                                            </div>
                                            <div className="modal-body">
                                                {error}
                                                <form>
                                                    <div className="form-group">
                                                        <label className="form-control-label">API Key</label>
                                                        <input name = "api_key" type = "text" value={this.state.api_key} className="form-control" onChange={(e) => this.changeAPIKey(e)}/>
                                                    </div>
                                                    <div className="form-group">
                                                        <label className="form-control-label">Password:</label>
                                                        <input name = "password" type = "password" value={this.state.password} className="form-control"  onChange={(e) => this.changePassword(e)} />
                                                    </div>
                                                    <div className="form-group">
                                                        <label className="form-control-label">Shop Name:</label>
                                                        <input name = "shop_name" type = "text" value={this.state.shop_name} className="form-control"  onChange={(e) => this.changeShopName(e)} />
                                                    </div>
                                                </form>
                                            </div>
                                            <div className="modal-footer">
                                                <button type="button" className="btn btn-default"
                                                        data-dismiss="modal">Close
                                                </button>
                                                <button type="button" id="submitHandler" className="btn btn-success" onClick={(e) => this.submitHandler(e)}>Save changes</button>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div className="panel-head">
                                    <div className="panel-title">
                                        <div className="panel-title-text">Marketplace Connection Center</div>
                                    </div>
                                </div>
                                            <div className="panel-wrapper">
                                                <div className="recent-list">
                                                    <ul>
                                                        <li>
                                                            <div className="tbl-cell main-icon"><img src="/static/images/amazon.png"
                                                                                         style={{"max-width": "90px"}}
                                                                                         alt=""/></div>
                                                            <div className="tbl-cell content">
                                                            <p><strong>Amazon Marketplace</strong></p>
                                                            <div className="action">
                                                                {this.state.seller_data_all && this.state.seller_data_all.seller_partner_status ?
                                                                    <ul>
                                                                        <li>Seller ID: {this.state.seller_data_all.seller_partner_id}</li>
                                                                        <li>Date Connected: {moment(this.state.seller_data_all.seller_partner_date_created).format('DD MMM YYYY')}</li>
                                                                        <li className="text-success">Connected</li>
                                                                    </ul>
                                                                    :
                                                                    <ul>
                                                                        <li>Seller ID: N/A</li>
                                                                        <li>Date Connected: N/A</li>
                                                                        <li className="text-warning">Not Connected</li>
                                                                        {/*<button type="button" className="btn btn-success" ></button>*/}
                                                                    </ul>

                                                                }
                                                            </div>
                                                </div>
                                                {this.state.seller_data_all &&this.state.seller_data_all.seller_partner_status ?
                                                <div className="tbl-cell download" style={{"text-align":"center; "}}>
                                                    <i className="icon-check text-success download"
                                                       style={{"font-size": "40px"}} />
                                                </div>
                                                    :
                                                    <div className="tbl-cell download" style={{"text-align":"center; "}}>
                                                                    <i className="icon-plus text-warning download" style={{"font-size": "40px"}} onClick={(e) => this.submitMarketplace(e)}/>
                                                                </div>
                                                }

                                            </li>
                                            <li>
                                                <div className="tbl-cell main-icon"><img src="/static/images/shopify.png"
                                                                                         style={{"max-width": "90px"}}
                                                                                         alt="" /></div>
                                                <div className="tbl-cell content">
                                                    <p><strong>Shopify Marketplace</strong></p>
                                                    <div className="action">


                                                             {shopify_account}

                                                        {/*<ul>*/}
                                                        {/*    <li>Seller ID: 454645541</li>*/}
                                                        {/*    <li>Date Connected: 12 April 2020</li>*/}
                                                        {/*    <li className="text-success">Connected</li>*/}
                                                        {/*</ul>*/}
                                                    </div>
                                                </div>
                                                 <div className="tbl-cell download" style={{"text-align":"center; "}}>
                                                    {/*<i className="icon-plus text-warning download"  data-toggle="modal"*/}
                                                    {/*   data-target="#modal4"*/}
                                                    {/*   style={{"font-size": "40px"}} />*/}
                                                     {shopify_account_status}
                                                    </div>

                                                {/*<div className="tbl-cell download" style={{"text-align":"center; "}}>*/}
                                                {/*    <i className="icon-check text-success download"*/}
                                                {/*       style={{"font-size": "40px"}}/>*/}
                                                {/*</div>*/}

                                            </li>
                                            <li>
                                                <div className="tbl-cell main-icon"><img src="/static/images/walmart.png"
                                                                                         style={{"max-width": "90px"}}
                                                                                         alt=""/></div>
                                                <div className="tbl-cell content">
                                                    <p><strong>Walmart Marketplace</strong></p>
                                                    <div className="action">
                                                        <ul>
                                                            <li>Seller ID: N/A</li>
                                                            <li>Date Connected: N/A</li>
                                                            <li className="text-warning">Not Connected</li>
                                                        </ul>
                                                    </div>
                                                </div>
                                                <div className="tbl-cell download" style={{"text-align":"center; "}}>
                                                    <i className="icon-plus text-warning download" data-toggle="modal" data-target="#myModal5"
                                                       style={{"font-size": "40px"}} />
                                                </div>

                                            </li>
                                            <li>
                                                <div className="tbl-cell main-icon"><img src="/static/images/newegg.png"
                                                                                         style={{"max-width": "90px"}}
                                                                                         alt="" /></div>
                                                <div className="tbl-cell content">
                                                    <p><strong>Newegg Marketplace</strong></p>
                                                    <div className="action">
                                                        {/*<ul>*/}
                                                        {/*    <li>Seller ID: SPEY23V545415</li>*/}
                                                        {/*    <li>Date Connected: 29 December, 2019</li>*/}
                                                        {/*    <li className="text-success">Connected</li>*/}
                                                        {/*</ul>*/}
                                                        <ul>
                                                            <li>Seller ID: N/A</li>
                                                            <li>Date Connected: N/A</li>
                                                            <li className="text-warning">Not Connected</li>
                                                        </ul>
                                                    </div>
                                                </div>
                                                 <div className="tbl-cell download" style={{"text-align":"center; "}}>
                                                    <i className="icon-plus text-warning download" data-toggle="modal" data-target="#myModal5"
                                                       style={{"font-size": "40px"}} />
                                                </div>
                                                {/*<div className="tbl-cell download" style={{"text-align":"center; "}}>*/}
                                                {/*    <i className="icon-plus text-success download"*/}
                                                {/*       style={{"font-size": "40px"}} />*/}
                                                {/*</div>*/}

                                            </li>

                                        </ul>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div className="col-md-4">
                            <div className="panel panel-default">
                                <div className="panel-head">
                                    <div className="panel-title">
                                        <div className="panel-title-text">Loan Renewal Eligibility</div>
                                    </div>
                                </div>
                                <div class Name="" style={{"padding-top": "0px;"}}>
                                    <div className="widget-1" style={{"padding-top": "0px;"}}>
                                        <div className="content" style={{"padding-top": "13px;"}}>
                                            <ul className="site-stat">
                                            <li>
                                                <h4 className="count">$ {this.state.loan_balance.balance ? (this.state.loan_balance.balance).toLocaleString('en') :"0.00"}</h4>
                                                <span className="country">Loan Balance</span>
                                                <div className="pull-right">Target: $ {this.state.loan_balance.balance_target ? (this.state.loan_balance.balance_target).toLocaleString('en') :"0.00"}</div>
                                                <div className="progress progress-md">
                                                    <div className="progress-bar bg-success" role="progressbar"
                                                         aria-valuenow="80" aria-valuemin="0" aria-valuemax="100"
                                                         style={{"width": loan_target_percentage+"%", "height": "16px;"}}><span className="sr-only">{this.state.loan_balance.target_percentage ? (this.state.loan_balance.target_percentage).toLocaleString('en') :"0"}%</span>{this.state.loan_balance.target_percentage ? (this.state.loan_balance.target_percentage).toLocaleString('en') :"0"}%
                                                    </div>
                                                </div>
                                            </li>
                                        </ul>
                                    </div>
                                    <div className="content">
                                        <ul className="site-stat">
                                            <li>
                                                <h4 className="count">{this.state.loan_history.loan_history_days ? this.state.loan_history.loan_history_days : 0} Days</h4>
                                                <span className="country">Loan History</span>
                                                <div className="pull-right">Target: {this.state.loan_history.loan_history_target ? this.state.loan_history.loan_history_target : 0} Days</div>
                                                <div className="progress progress-md">
                                                    <div className="progress-bar bg-success" role="progressbar"
                                                         aria-valuenow="80" aria-valuemin="0" aria-valuemax="100"
                                                         style={{"width": loan_history_percentage+'%', "height": "16px;"}}><span className="sr-only">80% Complete</span>{this.state.loan_history.history_target_percentage ? this.state.loan_history.history_target_percentage : 0}%
                                                    </div>
                                                </div>
                                            </li>
                                        </ul>
                                    </div>
                                    <div className="content">
                                        <ul className="site-stat">
                                            <li>
                                                <h4 className="count">{this.state.payment_success_history.success_percentage ? this.state.payment_success_history.success_percentage : 0}%</h4>
                                                <span className="country">Payment Success History</span>
                                                <div className="pull-right">Target: {this.state.payment_success_history.success_target_percentage ? this.state.payment_success_history.success_target_percentage : 0}%</div>
                                                <div className="progress progress-md">
                                                    <div className="progress-bar bg-success" role="progressbar"
                                                         aria-valuenow="98" aria-valuemin="0" aria-valuemax="100"
                                                         style={{"width": payment_percentage+"%", "height": "16px;"}}><span className="sr-only">98% Complete</span>{this.state.payment_success_history.target_percentage ? this.state.payment_success_history.target_percentage : 0}%
                                                    </div>
                                                </div>
                                            </li>
                                        </ul>
                                    </div>
                                </div>

                            </div>
                        </div>
                    </div>

                </div>

                </React.Fragment>
		);
	}
}
class LoanHistory extends  React.Component{
    constructor(props) {
        super(props);

       this.state = {
            data: [],
            loan: [],
            more_load:false,
            isDailyAdvanceLoading:true,
            seller_token : props ? props.seller_token :"",
            seller_data : props ? props.seller_data :"",
            seller_data_all : props ? props.seller_data_all :"",
            msg : "",
            new_loan_error_msg: "",
            eligible_amount:0,
            total_sales:0,
            total_eligible_amount:0,
            total_advance_available:0,
            total_gross_sales: 0,
            loan_applied_for_date: "",
            loan_applied_for_multiple_date: [],
            loan_fee_percentage: 0,
            loan_fee_amount: 0,
            checkbox_data:[],
            total_loan_applied: true,
            allChecked:false,
            eligibility:true,
            next: null,
            prev: null,
            is_payment_done: null,
        }

        this.changePage=this.changePage.bind(this);
        this.openModal = this.openModal.bind(this);
        this.loanApplied = this.loanApplied.bind(this);
        this.ChangeeligibleAmount = this.ChangeeligibleAmount.bind(this);
        this.submitNewLoan = this.submitNewLoan.bind(this);
        this.onExit = this.onExit.bind(this);
        this.handleAllChecked = this.handleAllChecked.bind(this);
        this.handleCheckChildElement = this.handleCheckChildElement.bind(this);
        this.setTotalLoanApplied = this.setTotalLoanApplied.bind(this);


	}

	componentWillReceiveProps(nextProps){
	    this.setState({
            seller_token : nextProps ? nextProps.seller_token :"",
            seller_data : nextProps ? nextProps.seller_data :"",
            seller_data_all : nextProps ? nextProps.seller_data_all :""
        })
    }

	componentWillMount() {
	    var auth = "Token "+ this.state.seller_token;
	    var url="/api/lastmonthdaywiseorders?limit=20";

        axios.get(url,{ 'headers': { 'Authorization': auth } })
            .then(res => {
                console.log('res=====================', res);
                this.setState({
                    data: res.data.results,
                    next:res.data.next,
                    prev:res.data.previous,
                    isDailyAdvanceLoading:false,

                });
            }).catch((err) => {
            this.setState({
                    isDailyAdvanceLoading:false,
            });
        });
    }
    ChangeeligibleAmount(e){
        // console.log(e);
        this.setState({
            loan_eligibility:this.state.loan_eligibility,
            applied_loan:this.state.eligible_amount,
            new_loan_error_msg:""

        });
    }

    getCookie(name){
            var cookieValue = null;
            if (document.cookie && document.cookie !== '') {
                var cookies = document.cookie.split(';');
                for (var i = 0; i < cookies.length; i++) {
                    var cookie = jQuery.trim(cookies[i]);
                    if (cookie.substring(0, name.length + 1) === (name + '=')) {
                        cookieValue = decodeURIComponent(cookie.substring(name.length + 1));
                        break;
                    }
                }
            }
        return cookieValue;
        }

    submitNewLoan(e){
        this.setState({
            isLoading:true
        })
         let auth1 = "Token "+ this.state.seller_token;
         let payment_check_url="/api/qpay-payment-check";

        axios.get(payment_check_url,{ 'headers': { 'Authorization': auth1 } })
            .then(res => {
                console.log('res=====================', res);
                this.setState({
                    is_payment_done: res.data.is_payment_done,
                    isLoading:false
                });
                if(res.data.is_payment_done){
                     //console.log("amt", this.state.total_eligible_amount, this.state.eligible_amount)
                    let amount = 0;
                    //console.log ("||||--",this.state.advance_id)
                    //console.log("|||| s1 rem after new rem", rem_after_pay, new_rem_after_pay)

                    if (this.state.total_eligible_amount === 0){
                        amount = this.state.eligible_amount
                        // console.log("amt 1", amount)
                    }
                    else{
                        amount = this.state.total_eligible_amount
                        // console.log("amt 2", amount)

                    }
                    let csrftoken = this.getCookie('csrftoken');
                    this.setState({
                        isLoading:true
                    })
                    e.preventDefault();
                    let url=document.location.origin+"/api/v1/qpay-loan-status/";
                    let auth="Token "+this.state.seller_token;
                    fetch(url,{
                        method:"POST",
                        headers: {
                                    'Accept': 'application/json',
                                    'Content-Type': 'application/json',
                                    'X-CSRFToken': csrftoken,
                                    'Authorization': auth
                                },
                        credentials: 'same-origin',
                         body:JSON.stringify({
                            // id:this.state.loan_id,
                            loan_period:"1",
                            loan_amount_approve: this.state.loan_amount_approve,
                            gross_sales_approve: this.state.total_gross_sales,
                            advace_approved: amount,
                            net_sales_approve: this.state.total_sales,
                            advance_rate_percentage: this.state.advance_rate_percentage,
                            reserve_rate_percentage: this.state.reserve_rate_percentage,
                            advance_id: this.state.advance_id,
                            reserve_amount: this.state.reserve_amount,
                            net_fee_approve: this.state.loan_fee_amount,
                            loan_fee_percentage: this.state.loan_fee_percentage,
                            loan_applied_for_date : this.state.loan_applied_for_date,
                            loan_applied_for_multiple_date : this.state.loan_applied_for_multiple_date,
                            loan_status:"Pending",

                        })
                    }).then(response=>{
                        if(response.status==201){
                            // console.log("Loan Request Submitted");
                            this.setState({
                                msg:"Loan Applied Successfully",
                                isLoading:false
                            });
                            const checkboxes = document.querySelectorAll(`input[name="checkbox"]`);

                        }
                        else if(response.status===406){
                            this.setState({
                                new_loan_error_msg:"You are not eligible for new loan please clear your previous dues",
                                isLoading:false

                            })
                        }
                        else{
                            this.setState({
                                new_loan_error_msg:"Error in Submission",
                                isLoading:false

                            })

                        }
                        location.reload();
                    });
                }
                else{
                    this.setState({
                                new_loan_error_msg:"Sorry.. you can not apply this loan because you haven't " +
                                    "made payment regarding your last settlement",
                                isLoading:false

                            })
                }
            }).catch((err) => {
            this.setState({
                     isLoading:false
            });
        });
    }

    onExit(event){
        const checkboxes = document.querySelectorAll(`input[name="checkbox"]`);
            checkboxes.forEach((checkbox) => {
                checkbox.checked = false;
            });        
        this.setState({       
            total_eligible_amount:0,
            total_sales:0,
            loan_fee_amount: 0,
            loan_fee_percentage: 0,
            advance_rate_percentage: 0,
            reserve_rate_percentage: 0,
            advance_id: 0,
            reserve_amount: 0,
            loan_amount_approve: 0,
            total_gross_sales:0,
            allchecked:false
        });

    }

    changePage(e){
        this.setState(
            {
                more_load:true,
            }
        )
    var url= e.currentTarget.dataset.link;
    var auth = "Token "+ this.state.seller_token;

            axios.get(url,{ 'headers': { 'Authorization': auth }})
            .then(res => {
                // console.log("----",res);
                var data=this.state.data;
                var b = res.data.results;
                var c= data.concat(b);
                this.setState({
                    data:c,
                    next:res.data.next,
                    prev:res.data.previous,
                    more_load:false,
                });
            }).catch((err) => {
                this.setState({
                    more_load:false,
                });
        });


    }

    openModal(e){
        var loan_applied_for_multiple_date = [];
        loan_applied_for_multiple_date.push(e.currentTarget.dataset.loan_applied_for_date)
        console.log("||||3 Advance Rate", e.currentTarget.dataset.advance_rate_percentage)
        console.log("||||4 Reserve Rate", e.currentTarget.dataset.reserve_amount)
        // console.log("single", loan_applied_for_multiple_date)
        
        this.setState({
            eligible_amount: e.currentTarget.dataset.eligible_amount,
            total_sales: e.currentTarget.dataset.total_sales,
            total_gross_sales: e.currentTarget.dataset.total_gross_sales,
            advance_rate_percentage: e.currentTarget.dataset.advance_rate_percentage,
            reserve_rate_percentage: e.currentTarget.dataset.reserve_rate_percentage,
            advance_id: e.currentTarget.dataset.advance_id,
            reserve_amount:e.currentTarget.dataset.reserve_amount,
            loan_amount_approve:e.currentTarget.dataset.loan_amount_approve,
            loan_fee_percentage: e.currentTarget.dataset.loan_fee_percentage,
            loan_fee_amount: e.currentTarget.dataset.loan_fee_amount,
            loan_applied_for_date: e.currentTarget.dataset.loan_applied_for_date,
            loan_applied_for_multiple_date: loan_applied_for_multiple_date
        })
        // console.log("single state", this.state.loan_applied_for_multiple_date)
    }

    loanApplied(){
        alert("You have already applied for this day");
    }

    setTotalLoanApplied(){
        this.state.data.map((value)=>{
            if(value.loan_applied === false){
                // console.log('getting false');
               this.setState({
                   total_loan_applied: false,
               })
            }

        })
    }

    handleAllChecked(event) {
        // to set global state for global apply now button
        this.setTotalLoanApplied();

        // console.log("event in handleallcheck::: ", event.target.checked, (clickedValue === 'selectAll') , document.querySelector(`input[value="SelectAll"]`));
        var clickedValue = event.target.value;
        const checkboxAll = document.querySelector(`input[value="SelectAll"]`);
        if(checkboxAll.checked===true) {
            // console.log("Here 1");
            this.setState({                
                allChecked: true
            })
        }else{
            clickedValue = false;
            // console.log("Here 2");            
            this.setState({                
                allChecked:false,
                total_sales:parseFloat("0").toFixed(2),
                total_eligible_amount:parseFloat("0").toFixed(2)
            })
        }

        let recent_settlement_obj = this.state.recent_settlement_obj
        const checkboxes = document.querySelectorAll(`input[name="checkbox"]`);
        var checked_data = [];
        var total_loan_eligible=0;
        var gross_sales =0;
        var advance_percentage = 0;
        var reserve_percentage = 0;
        var adv_id = 0;
        var reserve_amount_held = 0;
        var gross_loan_amount = 0;
        var advance_fee =0;
        var net_sales = 0;        
        var net_fee = 0;
        var loan_cost = 0;
        
            checkboxes.forEach((checkbox) => {
                var eligible_amount = checkbox.getAttribute("data-eligible_amount");
                var total_sales = checkbox.getAttribute("data-total_sales");
                var total_gross_sales = checkbox.getAttribute("data-total_gross_sales");
                var applied_for_date = checkbox.getAttribute("data-loan_applied_for_date");
                var is_loan_applied = checkbox.getAttribute("data-loan_applied");
                var loan_applied = checkbox.getAttribute("data-loan_applied");
                var advance_rate_percentage = checkbox.getAttribute("data-advance_rate_percentage");
                var reserve_rate_percentage = checkbox.getAttribute("data-reserve_rate_percentage");
                var advance_id = checkbox.getAttribute("data-advance_id");
                var reserve_amount = checkbox.getAttribute("data-reserve_amount");
                var loan_amount_approve = checkbox.getAttribute("data-loan_amount_approve");
                var loan_fee_percentage = checkbox.getAttribute("data-loan_fee_percentage");
                var loan_fee_amount = checkbox.getAttribute("data-loan_fee_amount");
                // console.log("eligible_amount", eligible_amount,"total_sales", total_sales, "total_gross_sales", total_gross_sales, "loan_fee_amount", loan_fee_amount, "loan_fee_percentage", loan_fee_percentage)
                checkbox.checked = event.target.checked
                if (event.target.checked === true)  {

                    if(is_loan_applied ==="false") {
                        // console.log("Here 3");
                        // console.log(loan_applied);
                        checked_data.push(checkbox.value);
                        total_loan_eligible = parseFloat(total_loan_eligible) + parseFloat(eligible_amount);
                        gross_sales = parseFloat(gross_sales) + parseFloat(total_gross_sales);
                        advance_percentage = parseFloat(advance_rate_percentage);
                        reserve_percentage = parseFloat(reserve_rate_percentage);
                        adv_id = parseFloat(advance_id);
                        reserve_amount_held = parseFloat(reserve_amount);
                        gross_loan_amount = parseFloat(loan_amount_approve);
                        advance_fee = parseFloat(loan_fee_percentage);
                        net_sales = parseFloat(net_sales) +  parseFloat(total_sales);
                        net_fee = parseFloat(net_fee) + parseFloat(loan_fee_amount);
                        loan_cost = parseFloat(loan_cost) + parseFloat(loan_fee_amount);                        
                        // console.log("|||||", applied_for_date);
                        // console.log("total_loan_eligible", total_loan_eligible, "gross_sales", gross_sales, "advance_fee", advance_fee, "net_sales", net_sales, "net_fee", net_fee)
                        if(this.state.loan_applied_for_multiple_date.includes(applied_for_date))
                        {
                           console.log("already exists")
                        }
                        else{
                            this.setState(previousState => ({
                                loan_applied_for_multiple_date: [...previousState.loan_applied_for_multiple_date, applied_for_date]
                            }));
                        }
                    }

                }
                else{
                     this.setState(previousState =>({
                        loan_applied_for_multiple_date: []
                    }));

                }
            });

            var checkboxobj = [];
            // console.log("Here 4");
            this.setState({                
                allChecked: true,
                checkbox_data: checked_data,
                total_eligible_amount:parseFloat(total_loan_eligible).toFixed(2),
                total_sales:parseFloat(net_sales).toFixed(2),
                loan_fee_amount: parseFloat(loan_cost).toFixed(2),
                advance_rate_percentage: parseFloat(advance_percentage).toFixed(2),
                reserve_rate_percentage: parseFloat(reserve_percentage).toFixed(2),
                advance_id: parseFloat(adv_id),
                reserve_amount: parseFloat(reserve_amount_held).toFixed(2),
                loan_amount_approve: parseFloat(gross_loan_amount).toFixed(2),
                loan_fee_percentage: parseFloat(advance_fee).toFixed(2),
                total_gross_sales:parseFloat(gross_sales).toFixed(2),
                loan_applied:true                

            });
            // console.log("checkbox.checked all ::::: ", checked_data);
            let moments = checked_data.map(d => moment(d));
            let latest_sales_date = moment.max(moments);
            let latest_sales_dt = moment(latest_sales_date).format("YYYY-MM-DD")
            console.log("latest_sales_date all::::: ", latest_sales_date);
            console.log("latest_sales_dt all::::: ", latest_sales_dt);
            this.setState({
                loan_applied_for_date:latest_sales_dt,
            })
      }

    handleCheckChildElement(e){

        // to set global state for global apply now button
        this.setTotalLoanApplied();

        var value=e.target.value;
        // console.log(value , "=====event in handlecheckfield::: ", e);
        const checkbox = document.querySelector(`input[value="`+value+`"]`);
        const checkboxAll = document.querySelector(`input[value="SelectAll"]`);

        const eligible_amount = e.currentTarget.dataset.eligible_amount;
        const total_sales = e.currentTarget.dataset.total_sales;
        const total_gross_sales = e.currentTarget.dataset.total_gross_sales;
        const advance_rate_percentage = e.currentTarget.dataset.advance_rate_percentage;
        const reserve_rate_percentage = e.currentTarget.dataset.reserve_rate_percentage;
        const advance_id = e.currentTarget.dataset.advance_id;
        const reserve_amount = e.currentTarget.dataset.reserve_amount;
        const loan_amount_approve = e.currentTarget.dataset.loan_amount_approve;
        const loan_fee_percentage = e.currentTarget.dataset.loan_fee_percentage;
        const loan_fee_amount = e.currentTarget.dataset.loan_fee_amount;
        const checkbox_data = this.state.checkbox_data;        
        const filtered_data=[];
        var total_loan_eligible = this.state.total_eligible_amount;
        var gross_sales = this.state.total_gross_sales;
        var net_sales = this.state.total_sales;
        var eligibility = this.state.eligibility;
        var advance_percentage = this.state.advance_rate_percentage;
        var adv_id = e.currentTarget.dataset.advance_id;
        var reserve_percentage = this.state.reserve_rate_percentage;
        var reserve_amount_held = this.state.reserve_amount;
        var gross_loan_amount = this.state.loan_amount_approve;
        var advance_fee = this.state.loan_fee_percentage;
        var advance_rate = this.state.loan_fee_amount;
        var applied_for_date = e.currentTarget.dataset.loan_applied_for_date;
        var is_loan_applied = e.currentTarget.dataset.loan_applied;
        var loan_applied = e.currentTarget.dataset.loan_apply;
        // console.log("applied date", applied_for_date);
        // console.log("is loan applied", is_loan_applied);
        console.log(loan_applied);
        // console.log("is_lo", is_loan_applied);
        if(checkbox.checked){
            if(is_loan_applied ==="false") {
                checkbox_data.push(value);               
                total_loan_eligible = parseFloat(total_loan_eligible) + parseFloat(eligible_amount);
                gross_sales = parseFloat(gross_sales) + parseFloat(total_gross_sales);
                net_sales = parseFloat(net_sales) + parseFloat(total_sales);
                advance_percentage = parseFloat(advance_rate_percentage);
                reserve_percentage = parseFloat(reserve_rate_percentage);
                adv_id = parseFloat(advance_id);
                reserve_amount_held = parseFloat(reserve_amount);
                gross_loan_amount = parseFloat(loan_amount_approve);
                advance_fee = parseFloat(loan_fee_percentage);
                advance_rate = parseFloat(advance_rate) + parseFloat(loan_fee_amount);
                // console.log(eligibility);
                eligibility = false;                       
                // console.log("||||| applied", applied_for_date);
                // console.log("gross sales", gross_sales, "net sales", net_sales, "total loan eligible", total_loan_eligible, "loan fee", advance_fee, "loan fee $", advance_rate, "eligibility", eligibility)
                // console.log("add", checkbox, parseFloat(total_loan_eligible));
                if (this.state.loan_applied_for_multiple_date.includes(applied_for_date) === false) {
                    // console.log(applied_for_date, "is not available");
                    this.setState(previousState => ({
                        loan_applied_for_multiple_date: [...previousState.loan_applied_for_multiple_date, applied_for_date]
                    }));
                }

                this.setState({
                    checkbox_data: checkbox_data,
                    total_eligible_amount: parseFloat(total_loan_eligible).toFixed(2),
                    total_gross_sales: parseFloat(gross_sales).toFixed(2),
                    total_sales: parseFloat(net_sales).toFixed(2),
                    advance_rate_percentage: parseFloat(advance_percentage).toFixed(2),
                    reserve_rate_percentage: parseFloat(reserve_percentage).toFixed(2),
                    advance_id: parseFloat(adv_id).toFixed(2),
                    reserve_amount: parseFloat(reserve_amount_held).toFixed(2),
                    loan_amount_approve: parseFloat(gross_loan_amount).toFixed(2),
                    loan_fee_percentage: parseFloat(advance_fee),
                    loan_fee_amount: parseFloat(advance_rate),
                    eligibility: eligibility

                });
                // console.log("|||||", eligibility);
            }

        }else{
            if(is_loan_applied ==="false") {                
                total_loan_eligible = parseFloat(total_loan_eligible) - parseFloat(eligible_amount);
                gross_sales = parseFloat(gross_sales) - parseFloat(total_gross_sales);
                net_sales = parseFloat(net_sales) - parseFloat(total_sales);
                advance_percentage = parseFloat(advance_rate_percentage);
                reserve_percentage = parseFloat(reserve_rate_percentage);
                adv_id = parseFloat(advance_id);
                reserve_amount_held = parseFloat(reserve_amount);
                gross_loan_amount = parseFloat(loan_amount_approve);
                advance_fee = parseFloat(loan_fee_percentage);
                advance_rate = parseFloat(advance_rate) - parseFloat(loan_fee_amount);
                const filtered_data = checkbox_data.filter(function (el) {
                    return el != value;
                });
                // console.log("remove", parseFloat(total_loan_eligible));
                // console.log("filtered_data", filtered_data, parseFloat(total_loan_eligible));
                // console.log(gross_sales, total_loan_eligible)
                checkboxAll.checked = false;
                this.setState({
                    allChecked: false,
                    checkbox_data: filtered_data,
                    total_eligible_amount: parseFloat(total_loan_eligible).toFixed(2),
                    total_gross_sales: parseFloat(gross_sales).toFixed(2),
                    advance_rate_percentage: parseFloat(advance_percentage).toFixed(2),
                    reserve_rate_percentage: parseFloat(reserve_percentage).toFixed(2),
                    advance_id: parseFloat(adv_id),
                    reserve_amount: parseFloat(reserve_amount_held).toFixed(2),
                    loan_amount_approve: parseFloat(gross_loan_amount).toFixed(2),
                    loan_fee_percentage: parseFloat(advance_fee),
                    loan_fee_amount: parseFloat(advance_rate),
                    total_sales: parseFloat(net_sales).toFixed(2)
                })
                if (this.state.loan_applied_for_multiple_date.includes(applied_for_date) === true) {
                    // loan_applied_for_multiple_date.pop(applied_for_date)
                    // console.log("ss", applied_for_date)
                    let get_element_index = this.state.loan_applied_for_multiple_date.indexOf(applied_for_date)
                    if (get_element_index > -1) {
                        this.state.loan_applied_for_multiple_date.splice(get_element_index, 1);
                    }
                }
            }
        }
        // console.log("checkbox.checked ::::: ", checkbox_data, value);
        let moments = checkbox_data.map(d => moment(d));
        let latest_sales_date = moment.max(moments);
        let latest_sales_dt = moment(latest_sales_date).format("YYYY-MM-DD")
        // console.log("latest_sales_date ::::: ", latest_sales_date);
        // console.log("latest_sales_dt ::::: ", latest_sales_dt);
        this.setState({
                loan_applied_for_date:latest_sales_dt,
            })
      }

    render() {
        // console.log("last_date", this.state.loan_applied_for_multiple_date);        
        let eligibility_status = true;
        let loan_fee_percentage = 2.0;
        let advance_rate_percentage = 60.0;
        let reserve_rate_percentage = 40.0;
        let advance_id = 1;
        let reserve_amount = 0;
        let loan_amount_approve = 0;
        let gross_item_sales = 0;        
        let sales_fees = 0;
        

        
        if(this.state.data && this.state.data[0] && this.state.data[0].eligibility){
            eligibility_status = this.state.data[0].eligibility;
            loan_fee_percentage = this.state.data[0].loan_fee_percentage;
            advance_rate_percentage = this.state.data[0].advance_rate_percentage;
            reserve_rate_percentage = this.state.data[0].reserve_rate_percentage;
            advance_id = this.state.data[0].id;
            reserve_amount = this.state.data[0].reserve_amount;
            loan_amount_approve =this.state.data[0].loan_amount_approve;
            gross_item_sales = this.state.data[0].amount;
            sales_fees = this.state.data[0].fees;            
            // console.log("||||", eligibility_status);
        }
        

        let new_loan_status=[];
        if(this.state.msg != ""){
            new_loan_status.push(<div class="alert alert-success">{this.state.msg}</div>);
        }
        if(this.state.new_loan_error_msg!=""){
           new_loan_status.push(<div class="alert alert-danger">{this.state.new_loan_error_msg}</div>);

        }

        var applyAll=[];
        // console.log("this.state.total_eligible_amount ::: ", this.state.total_eligible_amount);
        if(this.state.total_eligible_amount === 0 || this.state.total_eligible_amount === "0.00" || this.state.total_eligible_amount === null || this.state.total_eligible_amount === undefined ){
            // console.log("No Apply button");
            // applyAll.pop();
            applyAll=[]
        }else{
            // console.log("Apply button");

            if (this.state.total_loan_applied === false){
                applyAll.push(
                <div className="qp-apply-btn">
                    <a className="btn btn-primary  btn-shadow btn-pill "
                       data-toggle="modal"
                       data-total_eligible_amount={parseFloat(this.state.total_eligible_amount).toFixed(2)}
                       data-total_gross_sales={parseFloat(this.state.total_gross_sales).toFixed(2)}
                       data-total_sales={parseFloat(this.state.total_sales).toFixed(2)}
                       data-loan_fee_amount={parseFloat((this.state.total_sales) * (this.state.loan_fee_percentage * .01)).toFixed(2)}
                       gross_item_sales={parseFloat(this.state.gross_item_sales).toFixed(2)} 
                       data-advance_rate_percentage={parseFloat(this.state.advance_rate_percentage).toFixed(2)}
                       data-reserve_rate_percentage={parseFloat(this.state.reserve_rate_percentage).toFixed(2)}
                       data-advance_id={parseFloat(this.state.advance_id)} 
                       data-loan_amount_approve={parseFloat((this.state.total_sales) * (this.state.advance_rate_percentage * .01)).toFixed(2)}
                       data-reserve_amount={parseFloat(this.state.reserve_amount).toFixed(2)}                      
                       data-loan_fee_percentage={parseFloat(this.state.loan_fee_percentage).toFixed(2)}                                             
                       onClick={this.openModalSelected}
                       
                       data-target="#newLoan_selected"
                    ><i className="icon-plus mr-2"></i>Apply Now</a>
                </div>
            )
            }
            else{
                applyAll.push(
                <div className="qp-apply-btn">
                    <a className="btn btn-default  btn-shadow btn-pill "
                       data-toggle="modal"
                       data-total_eligible_amount={parseFloat(this.state.total_eligible_amount).toFixed(2)}
                       gross_item_sales={parseFloat(this.state.amount).toFixed(2)}
                        onClick={this.loanApplied}
                    ><i className="icon-plus mr-2"></i>Apply Now 2</a>
                </div>
            )
            }
        }

        var next=[];
        // var prev=[];

        // if(this.state.prev!=null){
        //     prev.push(
        //         <li className="paginate_button page-item active"><span data-link={this.state.prev} onClick={this.changePage}  style={{'cursor':'pointer', color: "#fff"}} class="page-link">&nbsp; <i class="fa fa-angle-double-left"></i>PREV</span></li>
        //         )
        // }
        if(this.state.next!=null){
            next.push(
                <li className="paginate_button page-item active"><span data-link={this.state.next} data-id="id" onClick={this.changePage} style={{'cursor':'pointer', color: "#fff"}} class="page-link">Next&nbsp; <i class="fa fa-angle-double-right"></i></span></li>
                )
        }
        return(
            <React.Fragment>
                <div className="row pt-4">
                    <div className="col-12">
                        <div className="panel panel-default">
                            <div className="panel-head">
                                <div className="panel-title">
                                    <div className="panel-title-text">Available Daily Advances</div>
                                </div>
                            </div>
                            {eligibility_status === false &&
                                    <div className="alert alert-warning">You are not eligible for new loan on this date . </div>
                                }
                            {eligibility_status === true &&
                            <div>
                                {/*<div className="panel-title">*/}
                                {/*    <div className="panel-title-text">Daily Advance Amount</div>*/}
                                {/*</div>*/}
                                <div className="modal fade text-left" data-backdrop="static" id="newLoan['+purchase_date+']">
                                    <div className="modal-dialog">
                                        <div className="modal-content">
                                            <div className="modal-header">
                                                <h4 className="modal-title">New Daily Advance </h4>
                                                <button type="button" className="close" data-dismiss="modal"
                                                        aria-label="Close"><span aria-hidden="true" onClick={this.onExit}>×</span>
                                                </button>
                                            </div>
                                            {new_loan_status}

                                            <div className="modal-body">
                                                <div className="form-group">
                                                    <label className=" col-form-label">Eligible Sales <i
                                                        className="tip tippy" data-tippy-animation="scale"
                                                        data-tippy-arrow="true" title="New loan"></i></label>                                                
                                                    <div class="input-group mb-3">
                                                        <div class="input-group-prepend">
                                                            <span class="input-group-text">$</span>
                                                        </div>
                                                        <input type="number"
                                                           className="form-control"
                                                           placeholder="Gross Sales"
                                                           value={this.state.total_sales ? parseFloat(this.state.total_sales).toFixed(2) : "0.00"}
                                                           readOnly required/>
                                                    </div>
                                                </div>                                              

                                                <div className="form-group">
                                                    <label className=" col-form-label">Daily Advance Amount <i
                                                        className="tip tippy" data-tippy-animation="scale"
                                                        data-tippy-arrow="true" title="New loan"></i></label>  
                                                    <div class="input-group mb-3">
                                                        <div class="input-group-prepend">
                                                            <span class="input-group-text">$</span>
                                                        </div>
                                                        <input type="number"
                                                           className="form-control"
                                                           placeholder="Eligible Amount"
                                                           value={this.state.eligible_amount}
                                                           readOnly required/>
                                                    </div>
                                                </div>

                                                <div className="form-group">
                                                    <label className="col-form-label">Cost of Advance <i
                                                        className="tip tippy" data-tippy-animation="scale"
                                                        data-tippy-arrow="true" title="Interest"></i></label>                                                
                                                    <div class="input-group mb-3">
                                                        <div class="input-group-prepend">
                                                            <span class="input-group-text">$</span>
                                                        </div>
                                                        <input type="number"
                                                           className="form-control"
                                                           placeholder="Eligible Amount"
                                                           value={this.state.eligible_amount ? parseFloat(this.state.total_sales  * loan_fee_percentage * 0.01).toFixed(2) : "0.00"}
                                                           readOnly required/>
                                                    </div>
                                                        
                                                </div>
                                                <div className="modal-footer">
                                                    <button type="button" className="btn btn-primary"
                                                            onClick={this.submitNewLoan}>Apply
                                                    </button>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                 <div className="modal fade text-left" data-backdrop="static" id="newLoan_selected">
                                    <div className="modal-dialog">
                                        <div className="modal-content">
                                            <div className="modal-header">
                                                {/*<h4 className="modal-title">New Loan {this.state.loan_applied_for_date}</h4>*/}
                                                <h4 className="modal-title">New Daily Advance (Batch) </h4>
                                                <button type="button" className="close" data-dismiss="modal" onClick={this.onExit}
                                                        aria-label="Close"><span aria-hidden="true">×</span>
                                                </button>
                                            </div>
                                            {new_loan_status}

                                            <div className="modal-body">
                                                <div className="form-group">
                                                    <label className=" col-form-label">Eligible Sales <i
                                                        className="tip tippy" data-tippy-animation="scale"
                                                        data-tippy-arrow="true" title="New loan"></i></label>
                                                    <div class="input-group mb-3">
                                                        <div class="input-group-prepend">
                                                            <span class="input-group-text">$</span>
                                                        </div>
                                                        <input type="number"
                                                           className="form-control"
                                                           placeholder="Total Sales Amount"
                                                           value={this.state.total_sales}
                                                           readOnly required/>
                                                    </div>                                                   
                                                </div>                                                
                                                <div className="form-group">
                                                    <label className=" col-form-label">Funding Available <i
                                                        className="tip tippy" data-tippy-animation="scale"
                                                        data-tippy-arrow="true" title="New loan"></i></label>
                                                    <div class="input-group mb-3">
                                                        <div class="input-group-prepend">
                                                            <span class="input-group-text">$</span>
                                                        </div>
                                                        <input type="number"
                                                           className="form-control"
                                                           placeholder="Eligible Amount"
                                                           value={this.state.total_eligible_amount}
                                                           readOnly required/>
                                                    </div>
                                                </div>
                                                <div className="form-group">
                                                    <label className="col-form-label">Cost of Funding <i
                                                        className="tip tippy" data-tippy-animation="scale"
                                                        data-tippy-arrow="true" title="Interest"></i></label>
                                                    <div class="input-group mb-3">
                                                        <div class="input-group-prepend">
                                                            <span class="input-group-text">$</span>
                                                        </div>
                                                        <input type="number"
                                                           className="form-control"
                                                           placeholder="Eligible Amount"
                                                           value={this.state.total_sales !== 0 ? this.state.loan_fee_amount : "0.00"}
                                                           readOnly required/>
                                                    </div>
                                                </div>
                                                <div className="modal-footer">
                                                    <button type="button" className="btn btn-primary"
                                                            onClick={this.submitNewLoan}>Apply
                                                    </button>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>



                            </div>
                            }
                            <div className="panel-body">
                                <div className="table-responsive">
                                    <table className="table table-qpay table-hover">
                                        <thead>
                                        <tr class="th-qpay">
                                            <th><input type="checkbox" onClick={this.handleAllChecked}  data-id="SelectAll" data-name="SelectAll" data-key="SelectAll"
                                                       value="SelectAll" data-ref="SelectAll" defaultChecked={this.state.allChecked }/></th>                                            
                                            <th>Order Date</th>
                                            <th>Gross Sales</th>
                                            {/*<th>Fees, Returns,<br/>and Holds</th>*/}
                                            {/*<th>Reserve Amount</th> */}
                                            <th>Returns</th>
                                            <th>Eligible Sales</th>
                                            {/*<th>Advance Fee</th> */}
                                            <th className="qp-table-adv">Advance Amount</th>
                                            {/*<th>Balance</th>*/}
                                            <th>{applyAll}</th>
                                        </tr>
                                        </thead>

                                        {this.state.isDailyAdvanceLoading ?
                                            <tbody>
                                                <div className="panel-body" align="center">
                                                     <ReactLoading type={'spin'} color={'blue'} height={'50%'} width={'50%'} />
                                                </div>
                                            </tbody>
                                             :
                                            <tbody>
                                                {this.state.data.map((item, key)=>
                                                    item.eligibility === true &&  (                                                     
                                                    <tr>                                                                                                                                                                                                                               
                                                        <td>
                                                        {item.loan_applied === false ?                                                            
                                                            <input type="checkbox" 
                                                            data-id={item.purchase_date}
                                                            data-gross-sales
                                                            data-total_sales={parseFloat(item.amount - item.fees).toFixed(2)}
                                                            data-total_gross_sales={item.amount}
                                                            data-advance_rate_percentage={item.advance_rate_percentage}
                                                            data-reserve_rate_percentage={item.reserve_rate_percentage} 
                                                            data-advance_id={parseFloat(item.id)}                                                           
                                                            data-reserve_amount={parseFloat((item.amount - item.fees) * item.reserve_rate_percentage * .01).toFixed(2)}
                                                            data-loan_fee_percentage={item.loan_fee_percentage}
                                                            data-loan_fee_amount={parseFloat((item.amount - item.fees) * (item.loan_fee_percentage * .01)).toFixed(2)}
                                                            data-eligible_amount={parseFloat((item.amount - item.fees) * item.advance_rate_percentage * .01).toFixed(2)}
                                                            data-loan_amount_approve={parseFloat(((item.amount - item.fees) * item.advance_rate_percentage * .01)+(item.amount - item.fees) * (item.loan_fee_percentage * .01)).toFixed(2)}
                                                            data-loan_applied_for_date={item.purchase_date}
                                                            data-loan_applied={item.loan_applied}
                                                            name="checkbox" key={item.purchase_date}
                                                            value={item.purchase_date} ref={item.purchase_date}
                                                            onClick={this.handleCheckChildElement}
                                                            />
                                                            :
                                                            <div><i className="icon-lock text-success" style={{"font-size": "24px"}}></i></div>
                                                        }
                                                        </td>
                                                        <td>{item.purchase_date ? <i className="far fa-clock mr-2 text-muted"></i> : ''} {moment(item.purchase_date).format("MMMM-DD-YYYY")}</td>
                                                        <td>$ {(parseFloat(item.amount)).toFixed(2).replace(/\d(?=(\d{3})+\.)/g, '$&,')}</td>
                                                        {/*<td>$ {(parseFloat(item.fees)).toFixed(2)}</td>*/}
                                                        <td>$ { item.return_or_credit? item.return_or_credit : 0.00 }</td>
                                                        {/*<td>$ {((item.amount - item.fees) * item.reserve_rate_percentage * .01).toFixed(2).replace(/\d(?=(\d{3})+\.)/g, '$&,')}</td> */}
                                                        <td>$ {(item.amount - item.return_or_credit).toFixed(2).replace(/\d(?=(\d{3})+\.)/g, '$&,')}</td>
                                                        {/*<td>$ {(((item.amount - item.fees) * item.loan_fee_percentage) * .01).toFixed(2).replace(/\d(?=(\d{3})+\.)/g, '$&,')}</td> */}
                                                        <td className="qp-table-adv">$ {((item.amount - item.fees) * item.advance_rate_percentage * .01).toFixed(2).replace(/\d(?=(\d{3})+\.)/g, '$&,')}</td>
                                                        {/*<td>$ {(parseFloat(item.balance)).toFixed(2)}</td>*/}
                                                        <td>
                                                            {this.state.total_eligible_amount === 0 ||
                                                            this.state.total_eligible_amount === "0.00" ||
                                                            this.state.total_eligible_amount === null ||
                                                            this.state.total_eligible_amount === undefined ?
                                                                <div>
                                                            {item.loan_applied === false ?
                                                                    <div className="qp-apply-btn">
                                                                        <a className="btn btn-primary  btn-shadow btn-pill"
                                                                           data-toggle="modal"
                                                                           data-total_gross_sales={parseFloat(item.amount).toFixed(2)}
                                                                           data-total_sales={parseFloat(item.amount - item.fees).toFixed(2)}  
                                                                           data-advance_rate_percentage={parseFloat(item.advance_rate_percentage).toFixed(2)} 
                                                                           data-reserve_rate_percentage={parseFloat(item.reserve_rate_percentage).toFixed(2)}
                                                                           data-advance_id={parseFloat(item.id)}  
                                                                           data-reserve_amount={parseFloat((item.amount - item.fees) * item.reserve_rate_percentage * .01).toFixed(2)}                                                                        
                                                                           data-loan_fee_percentage={parseFloat(item.loan_fee_percentage).toFixed(2)}
                                                                           data-loan_fee_amount={parseFloat((item.amount - item.fees) * (item.loan_fee_percentage * .01)).toFixed(2)}                                                                           
                                                                           data-eligible_amount={parseFloat((item.amount - item.fees) * item.advance_rate_percentage * .01).toFixed(2)}
                                                                           data-loan_amount_approve={parseFloat(((item.amount - item.fees) * item.advance_rate_percentage * .01)+(item.amount - item.fees) * (item.loan_fee_percentage * .01)).toFixed(2)}                                                                           
                                                                           data-loan_applied_for_date={item.purchase_date} onClick={this.openModal}
                                                                           data-target="#newLoan['+purchase_date+']"><i className="icon-plus mr-2"></i>Apply Now</a>
                                                                    </div>
                                                              :
                                                                    <div className="qp-apply-btn">
                                                                        <a className="btn btn-default  btn-shadow btn-pill "
                                                                           data-toggle="modal"
                                                                           data-total_gross_sales={parseFloat(item.amount).toFixed(2)}
                                                                           data-total_sales={parseFloat(item.amount - item.fees).toFixed(2)} 
                                                                           data-advance_rate_percentage={parseFloat(item.advance_rate_percentage).toFixed(2)}   
                                                                           data-reserve_rate_percentage={parseFloat(item.reserve_rate_percentage).toFixed(2)}
                                                                           data-advance_id={parseFloat(item.id)}
                                                                           data-reserve_amount={parseFloat((item.amount - item.fees) * item.reserve_rate_percentage * .01).toFixed(2)}                                                                       
                                                                           data-loan_fee_percentage={parseFloat(item.loan_fee_percentage).toFixed(2)}
                                                                           data-loan_fee_amount={parseFloat((item.amount - item.fees) * (item.loan_fee_percentage * .01)).toFixed(2)}                                                                           
                                                                           data-eligible_amount={parseFloat((item.amount - item.fees) * item.advance_rate_percentage * .01).toFixed(2)}
                                                                           data-loan_amount_approve={parseFloat(((item.amount - item.fees) * item.advance_rate_percentage * .01)+(item.amount - item.fees) * (item.loan_fee_percentage * .01)).toFixed(2)}                                                                           
                                                                           data-loan_applied_for_date={item.purchase_date} onClick={this.loanApplied}>
                                                                           <i className="icon-lock mr-2"></i>Advance Active</a>
                                                                    </div>
                                                            }
                                                            </div>
                                                            :<div></div>}
                                                        </td>
                                                    </tr>)
                                                )}
                                                </tbody>

                                                }

                                    </table>
                                </div>
                                {this.state.more_load ? <div className="panel-body" align="center">
                                     <ReactLoading type={'spin'} color={'blue'} height={'5%'} width={'5%'} />
                                </div>
                                    : ""
                                }
                                <nav className="navigation pagination-container text-center" role="navigation">
                                <ul className="pagination">
                                    {next}
                                </ul>
                        </nav>
                            </div>
                        </div>
                    </div>
                </div>
            </React.Fragment>
        )
    }
}

export class Popup extends React.Component{
    constructor() {
        super();
    }

    render() {
        return(
            <div>
            <div className="container share-mod popfive">
                <div className="modal" id="myModal5">
                    <div className="modal-dialog">
                        <div className="modal-content">
                            <div className="modal-body">
                                <button type="button" id="dismiss_log" className="close" data-dismiss="modal">
                                    <img src="/static/images/cross-menu.png" width="16px"/>
                                </button>
                                <div className="row">
                                    <div className="col">
                                        <h3>MarketPlace Connection Status</h3>
                                        <div className="row  pt-4">
                                            <div className="col new">
                                               <p className="text-warning ">Coming Soon</p>
                                            </div>

                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            </div>
        )
    }
}

export default QuickPayContainer;
